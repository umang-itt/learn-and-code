#include<bits/stdc++.h>
using namespace std;

int main(){

	queue<int>spidersQueue;
	int totalSpider,spiderToSelect,spidersArray[1000000],count=0;

	cin>>totalSpider;
	cin>>spiderToSelect;				
			
	for(int iterator=0; iterator<totalSpider; iterator++)
	{
		cin>>spidersArray[iterator];
		spidersQueue.push(iterator);
	}
	
	while(count<spiderToSelect)
	{
		int maxPower=-1,maxIndex,selectedSpidersPower[spiderToSelect+1];
		int size=spidersQueue.size();
		for(int iterator=0; iterator<min(spiderToSelect,size); iterator++)
		{
			selectedSpidersPower[iterator]=spidersQueue.front();
			spidersQueue.pop();
			if(spidersArray[selectedSpidersPower[iterator]]>maxPower)
			{
				maxPower=spidersArray[selectedSpidersPower[iterator]];
				maxIndex=selectedSpidersPower[iterator];
			}
		}
		cout<<maxIndex+1<<" ";
		for(int iterator=0; iterator<min(spiderToSelect,size); iterator++)
		{
			if(selectedSpidersPower[iterator]==maxIndex){

				continue;
			}
			else{

				if(spidersArray[selectedSpidersPower[iterator]]){

					spidersArray[selectedSpidersPower[iterator]]--;
				}
				spidersQueue.push(selectedSpidersPower[iterator]);
			}
		}
		count++;
	}
	return 0;
}